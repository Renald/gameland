<?php

namespace App\Form;

use App\Entity\Jeux;
use App\Entity\TypeJeux;
use App\Entity\Console;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;



class GestionJeuxType extends AbstractType 
{
   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom')
            ->add('Type_de_jeux', EntityType::class,[
                'required' => true,
                'placeholder' => 'Choississez un Type de jeux..',
                'class' => TypeJeux::class,
                'choice_label' => 'Nom_du_type'])
            ->add('Console', EntityType::class,[
                'required' => true,
                'placeholder' => 'Choississez une Console..',
                'class' => Console::class,
                'choice_label' => 'Nom'])
            ->add('Date_creation',DateType::class,[
                'widget' =>'single_text',
                'html5' => false,
                'format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'js-datepicker']
            ])
            ->add('Editeur')
            ->add('Developpeur')
            ->add('Description')
            ->add('image1',FileType::class,['label' => 'Image 1','required' => false,'data_class' => null])
            ->add('image2', FileType::class,['label' => 'Image 2','required' => false,'data_class' => null])
            ->add('image3', FileType::class,['label' => 'Image 3','required' => false,'data_class' => null])
            ->add('image4', FileType::class,['label' => 'Image 4','required' => false,'data_class' => null])
            ->add('video')
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jeux::class,
        ]);
    }
}
