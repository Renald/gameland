<?php

namespace App\Form;

use App\Entity\Console;
use App\Entity\Fabricant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use Symfony\Bridge\Doctrine\Form\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class GestionConsoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      
        $builder
            ->add('Nom')
            ->add('Annee_de_creation',DateType::class,[
                'widget' =>'single_text',
                'html5' => false,
                'format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'js-datepicker']
            ])
            ->add('Description')
            ->add('Fabricant', EntityType::class,[
                'required' => true,
                'placeholder' => 'Choississez un Fabricant..',
                'class' => Fabricant::class,
                'choice_label' => 'libelle'
            ])
            ->add('Image_console',FileType::class,['label' => 'Image de la console','required' => false,'data_class' => null])
            ->add('Image_logo', FileType::class,['label' => 'Logo', 'required' => false,'data_class' => null])
           
       
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Console::class,
        ]);
    }
}
