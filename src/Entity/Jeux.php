<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\JeuxRepository")
 */
class Jeux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TypeJeux", inversedBy="idType", cascade={"persist", "remove"})
     */
    private $Type_de_jeux;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $Date_creation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Console", inversedBy="idConsole", cascade={"persist", "remove"})
     */
    private $Console;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Editeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Developpeur;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getTypeDeJeux(): ?TypeJeux
    {
        return $this->Type_de_jeux;
    }

    public function setTypeDeJeux(?TypeJeux $Type_de_jeux): self
    {
        $this->Type_de_jeux = $Type_de_jeux;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->Date_creation;
    }

    public function setDateCreation(?\DateTimeInterface $Date_creation): self
    {
        $this->Date_creation = $Date_creation;

        return $this;
    }

    public function getConsole(): ?Console
    {
        return $this->Console;
    }

    public function setConsole(?Console $Console): self
    {
        $this->Console = $Console;

        return $this;
    }

    public function getEditeur(): ?string
    {
        return $this->Editeur;
    }

    public function setEditeur(?string $Editeur): self
    {
        $this->Editeur = $Editeur;

        return $this;
    }

    public function getDeveloppeur(): ?string
    {
        return $this->Developpeur;
    }

    public function setDeveloppeur(?string $Developpeur): self
    {
        $this->Developpeur = $Developpeur;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getImage1(): ?string
    {
        return $this->image1;
    }

    public function setImage1(?string $image1): self
    {
        $this->image1 = $image1;

        return $this;
    }

    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function getImage3(): ?string
    {
        return $this->image3;
    }

    public function setImage3(?string $image3): self
    {
        $this->image3 = $image3;

        return $this;
    }

    public function getImage4(): ?string
    {
        return $this->image4;
    }

    public function setImage4(?string $image4): self
    {
        $this->image4 = $image4;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }
}
