<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConsoleRepository")
 */
class Console
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nom;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $Annee_de_creation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Fabricant", inversedBy="Console")
     */
    private $Fabricant;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image_console;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image_logo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Jeux", mappedBy="Console", cascade={"persist", "remove"})
     */
    private $idConsole;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(?string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getAnneeDeCreation(): ?\DateTimeInterface
    {
        return $this->Annee_de_creation;
    }

    public function setAnneeDeCreation(?\DateTimeInterface $Annee_de_creation): self
    {
        $this->Annee_de_creation = $Annee_de_creation;

        return $this;
    }

    public function getFabricant(): ?Fabricant
    {
        return $this->Fabricant;
    }

    public function setFabricant(?Fabricant $Fabricant): self
    {
        $this->Fabricant = $Fabricant;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getImageConsole(): ?string
    {
        return $this->Image_console;
    }

    public function setImageConsole(?string $Image_console): self
    {
        $this->Image_console = $Image_console;

        return $this;
    }

    public function getImageLogo(): ?string
    {
        return $this->Image_logo;
    }

    public function setImageLogo(?string $Image_logo): self
    {
        $this->Image_logo = $Image_logo;

        return $this;
    }

    public function getIdConsole(): ?Jeux
    {
        return $this->idConsole;
    }

    public function setIdConsole(?Jeux $idConsole): self
    {
        $this->idConsole = $idConsole;

        // set (or unset) the owning side of the relation if necessary
        $newConsole = $idConsole === null ? null : $this;
        if ($newConsole !== $idConsole->getConsole()) {
            $idConsole->setConsole($newConsole);
        }

        return $this;
    }
}
