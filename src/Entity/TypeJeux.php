<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeJeuxRepository")
 */
class TypeJeux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_du_type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Jeux", mappedBy="Type_de_jeux", cascade={"persist", "remove"})
     */
    private $idType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDuType(): ?string
    {
        return $this->Nom_du_type;
    }

    public function setNomDuType(string $Nom_du_type): self
    {
        $this->Nom_du_type = $Nom_du_type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getIdType(): ?Jeux
    {
        return $this->idType;
    }

    public function setIdType(?Jeux $idType): self
    {
        $this->idType = $idType;

        // set (or unset) the owning side of the relation if necessary
        $newType_de_jeux = $idType === null ? null : $this;
        if ($newType_de_jeux !== $idType->getTypeDeJeux()) {
            $idType->setTypeDeJeux($newType_de_jeux);
        }

        return $this;
    }
}
