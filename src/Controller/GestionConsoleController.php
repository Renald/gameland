<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Console;
use App\Entity\Fabricant;
use App\Form\GestionConsoleType;
use Symfony\Component\HttpFoundation\Request;
use App\Knp\Bundle\PaginatorBundle;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GestionConsoleController extends Controller
{

      /**
     * @Route("/gestion/DeleteConsole/{id}", name="delete_console")
     */
    public function DeleteFabricant(Console $Console){

        $EntityManager = $this->getDoctrine()->getManager();
        $EntityManager->remove($Console);
        $EntityManager->flush();

        $this->addFlash('success', 'La Console à bien été supprimé');
        return $this->redirectToRoute('liste_console');
    }


    /**
     * @Route("/gestion/ListeConsole", name="liste_console")
     */
    public function ListeConsole(Request $request)
    {
        $liste = $this->getDoctrine()->getRepository(Console::class);
        $findAll = $liste->findAll();
        $count = count($findAll);


        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $Console = $paginator->paginate(
            // Doctrine Query, not results
            $findAll,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );




        return $this->render("gestion_console/listeConsole.html.twig",[
            'Console' => $Console,
            'count' => $count
        ]);
    }

    /**
     * @Route("/gestion/RechercheConsole", name="recherche_console")
     */
    public function RechercheConsole (Request $request)
    {
        $val = $request->request->get('value');
        $recherche = $this->getDoctrine()->getRepository(Console::class);
        $query = $recherche->createQueryBuilder('u');
        $query->where('u.Nom like :item')
              ->setParameter('item', '%'.$val.'%');

         $sql = $query->getQuery();
         $find = $sql->getResult();     

       // $find  = $recherche->findBy(['Nom' => $val]);
        $count = count($find);

        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $Console = $paginator->paginate(
            // Doctrine Query, not results
            $find,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );
        return $this->render("gestion_console/listeSubConsole.html.twig",[
            'Console' => $Console,
            'count' => $count
        ]);


    }
    /**
     * @Route("/gestion/GestionConsole", name="ajout_console")
     * @Route("/gestion/GestionConsole/{id}", name="modif_console")
     */
    public function index(Console $Console = null,Request $request)
    {
        if(!$Console)
        $Console = new Console();
        
        $form = $this->createForm(GestionConsoleType::class,$Console);

        $form->handleRequest($request);





        if($form->isSubmitted() && $form->isValid()){

            $Console = $form->getData();
            //$Console->setAnneeDeCreation( DateTime::createFromFormat('d/m/Y', $form->getAnneeDeCreation())->format('Y-m-d'));
            if($imageConsole = $request->request->get('fileconsole'))
                    $Console->setImageConsole($imageConsole);

            if($imageLogo = $request->request->get('filelogo'))
                $Console->setImageLogo($imageLogo);        
           $EntityManager = $this->getDoctrine()->getManager();
           $EntityManager->persist($Console);
           $EntityManager->flush();

            $this->addFlash("success","La console ".$Console->getNom()." à été creéée");
          //$this->addFlash("success","La console ".$request->request->get('Annee_de_creation')." à été creéée");
           // return $this->redirectToRoute("liste_console");
        }

        return $this->render('gestion_console/index.html.twig', [
            'controller_name' => 'GestionConsoleController',
            'form' => $form->createView(),
            'Modif' =>$Console->getId() != null,
            'Image_console' => $Console->getImageConsole(),
            'Image_logo' => $Console->getImageLogo()
            
        ]);
    }
}
