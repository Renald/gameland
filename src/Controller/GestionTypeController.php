<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\TypeJeux;
use App\Form\GestionTypeJeuxType;
use App\Repository\TypeJeuxRepository;
use App\Knp\Bundle\PaginatorBundle;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GestionTypeController extends Controller
{

    /**
     * @Route("/gestion/DeleteType/{id}", name="delete_type")
     */
    public function DeleteType(Request $request,TypeJeux $TypeJeux)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($TypeJeux);
        $entityManager->flush();


        $this->addFlash('success', 'Le Type de Jeux '.$TypeJeux->getNomDuType().' à bien été supprimé');
        return $this->redirectToRoute('liste_type');


    }

    /**
     * @Route("/gestion/GestionType", name="ajout_type")
     * @Route("/gestion/GestionType/{id}", name="modif_type")
     */
    public function index(Request $request, TypeJeux $TypeJeux = null)
    {
        if(!$TypeJeux)
            $TypeJeux = new TypeJeux();

        $form = $this->createForm(GestionTypeJeuxType::class, $TypeJeux);    

        $form->handlerequest($request);


        if($form->isSubmitted() && $form->isValid())
        {
            $TypeJeux = $form->getData();
            $repo = $this->getDoctrine()->getRepository(TypeJeux::class);
            $findType = $repo->findBy(array(
                'Nom_du_type' => $TypeJeux->getNomDuType()
            ));
            
            if(!$TypeJeux->getId() && count($findType) >0){
                $this->addFlash('error', 'Le Type de Jeux '.$TypeJeux->getNomDuType().' existe déjà ');
            }
            else{
                 if(!$TypeJeux->getId())
                 $this->addFlash('success', 'Le Type de Jeux '.$TypeJeux->getNomDuType().' à bien été crée');
                else
                $this->addFlash('success', 'Le Type de Jeux '.$TypeJeux->getNomDuType().' à bien été Mdifié'); 
            
           
                $EntityManager = $this->getDoctrine()->getManager();
                $EntityManager->persist($TypeJeux);
                $EntityManager->flush();

                return $this->redirectToRoute('liste_type');
            }   

        }

        return $this->render('gestion_type/index.html.twig', [
            'form' => $form->CreateView(),
            'Modif' =>$TypeJeux->getId() != null,
        ]);
    }

    /**
     * @Route("/gestion/liste_type" , name="liste_type")
     */

     public function ListeType(Request $request)
     {

        $repo = $this->getDoctrine()->getRepository(TypeJeux::class);
        $findAll = $repo->findAll();
        $count = count($findAll);
        
        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $TypeJeux = $paginator->paginate(
            // Doctrine Query, not results
            $findAll,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );



        return $this->render('gestion_type/listeType.html.twig', [
            'TypeJeux' => $TypeJeux,
            'count' =>$count,
        ]);

     }
     
}
