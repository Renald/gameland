<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Fabricant;
use App\Service\FileUploader;
use App\Form\GestionFabricantType;
use App\Repository\FabricantRepository;
use App\Knp\Bundle\PaginatorBundle;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class GestionFabricantController extends Controller
{

    /**
     * @Route("/gestion/DeleteFabricant/{id}", name="delete_Fabricant")
     */
    public function DeleteFabricant(Fabricant $Fabricant){

        $EntityManager = $this->getDoctrine()->getManager();
        $EntityManager->remove($Fabricant);
        $EntityManager->flush();

        $this->addFlash('success', 'Le Fabricant à bien été supprimé');
        return $this->redirectToRoute('liste_fabricant');
    }

     /**
     * @Route("/gestion/ListeFabricant", name="liste_fabricant")
     */
    public function ListeFabricant(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Fabricant::class);
        $AllFabricant = $repo->findAll();  
        $count = count($AllFabricant);
        
        

        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $Fabricant = $paginator->paginate(
            // Doctrine Query, not results
            $AllFabricant,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );


        return $this->render('gestion_fabricant/listeFabricant.html.twig', [
            'Fabricant' => $Fabricant,
            'count' => $count,
        ]);

    } 


       /**
     * @Route("/gestion/RechercheFabricant", name="recherche_fabricant")
     */
    public function RechercheFabricant (Request $request)
    {
        $val = $request->request->get('value');
        $recherche = $this->getDoctrine()->getRepository(Fabricant::class);
        $query = $recherche->createQueryBuilder('u');
        $query->where('u.libelle like :item')
              ->setParameter('item', '%'.$val.'%');

         $sql = $query->getQuery();
         $find = $sql->getResult();     

       // $find  = $recherche->findBy(['Nom' => $val]);
        $count = count($find);

        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $Fabricant = $paginator->paginate(
            // Doctrine Query, not results
            $find,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );
        return $this->render("gestion_fabricant/listeSubFabricant.html.twig",[
            'Fabricant' => $Fabricant,
            'count' => $count
        ]);


    }

    /**
     * @Route("/gestion/Gestionfabricant", name="ajout_fabricant")
     * @Route("/gestion/Gestionfabricant/{id}", name="modif_fabricant")
     */
    public function GestionFabricant(Fabricant $Fabricant = null, Request $request)
    {
        if(!$Fabricant)
            $Fabricant = new Fabricant();

        $form = $this->createForm(GestionFabricantType::class, $Fabricant);

    
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            if(!$Fabricant->getId())
                $Fabricant->setDate(new \DateTime('now'));


            $Fabricant = $form->getData();
                if($image = $request->request->get('filelogo'))
                    $Fabricant->setImage($image);
            //$file = $form->get('Image')->getData();
            $repo = $this->getDoctrine()->getRepository(Fabricant::class);
            $findFabricant = $repo->findBy(array(
                'libelle' => $Fabricant->getLibelle()
            )) ;
           if(!$Fabricant->getId() && count($findFabricant) >0){
                 $this->addFlash('error', 'Le Fabricant '.$Fabricant->getLibelle().' existe déjà');  
                 return $this->redirectToRoute('liste_fabricant');
          
          }else{

                  if(!$Fabricant->getId())
                       $this->addFlash('success', 'Le Fabricant '.$Fabricant->getLibelle().' à bien été crée');
                    else
                        $this->addFlash('success', 'Le Fabricant '.$Fabricant->getLibelle().' à bien été Modifié'); 

                     $EntityManager = $this->getDoctrine()->getManager();
                     $EntityManager->persist($Fabricant);
                     $EntityManager->Flush(); 
                
              

                return $this->redirectToRoute('liste_fabricant');
           }  
        }

        return $this->render('gestion_fabricant/index.html.twig', [
            'form' => $form->createView(),
            'Modif' => $Fabricant->getId()!= null,
            'image' => $Fabricant->getImage(),
        ]);
    }

    /**
     * @Route("/uploadImage" ,name="upload_image")
     */
    public function uploadImage(Request $req){
       /* $Upload = new FileUploader($this->getParameter('Fabricant'));
            if($Upload->Upload($filname) == 0){
                $this->addFlash('error','Une erreur s\' est produite lors de l\'envoie du fichier');
               return response('file_ok');
            }*/
            if($req->isXMLHttpRequest())  {  
               
                $filename = $req->files->get('file');
                $status = array('status' => "error","fileUploaded" => false);
                $Upload = new FileUploader($this->getParameter('Fabricant'));
                $newfilename = $Upload->Upload($filename);
                if($newfilename['up']){
                    $status = array('status' => "success","fileUploaded" => true,"newfilename" => $newfilename['filename']);
                   
                } else{
                    $status = array('status' => "error","fileUploaded" => false,"newfilename" => $newfilename['message']);
                }
           
                return new JsonResponse($status);
            }
    
    }

     
   


}
