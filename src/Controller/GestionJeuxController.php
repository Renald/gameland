<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Jeux;
use App\Entity\TypeJeux;
use App\Entity\Console;
use App\Service\FileUploader;
use App\Form\GestionJeuxType;
use App\Repository\JeuxRepository;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Knp\Bundle\PaginatorBundle;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class GestionJeuxController extends Controller
{

      /**
     * @Route("/gestion/DeleteJeux/{id}", name="delete_jeux")
     */
    public function DeleteJeux(Request $request,Jeux $jeux)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($jeux);
        $entityManager->flush();


        $this->addFlash('success', 'Le Type de Jeux '.$jeux->getNom().' à bien été supprimé');
        return $this->redirectToRoute('liste_type');


    }


    /**
     * @Route("/gestion/GestionJeux", name="ajout_jeux")
     * @Route("/gestion/GestionJeux/{id}", name="modif_jeux")
     */
    public function index(Request $request,Jeux $jeux = null)
    {
        if(!$jeux ){
            $jeux = new Jeux();
        }
        
        $form = $this->CreateForm(GestionJeuxType::class, $jeux);

        $form->handlerequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $Jeux = $form->getData();
          
            if($image1 = $request->request->get('fileimage1'))
                    $Jeux->setImage1($image1);

            if($image2 = $request->request->get('fileimage2'))
                $Jeux->setImage2($image2); 
                
            if($image3 = $request->request->get('fileimage3'))
                $Jeux->setImage3($image3);

            if($image4 = $request->request->get('fileimage4'))
            $Console->setImage4($image4);

            $EntityManager = $this->getDoctrine()->getManager();
            $EntityManager->persist($Jeux);
            $EntityManager->flush();
 
             $this->addFlash("success","Le jeux ".$Jeux->getNom()." à été créé");


        }

        return $this->render('gestion_jeux/index.html.twig', [
            'form' =>$form->createView(),
            'Modif' =>$jeux->getId() != null,
            'Image1' => $jeux->getImage1(),
            'Image2' => $jeux->getImage2(),
            'Image3' => $jeux->getImage3(),
            'Image4' => $jeux->getImage4()
            
        ]);
    }

    /**
     * @Route ("/gestion/listeJeux", name="liste_jeux")
     */
    public function listeJeux (Request $request)
    {

        $repo = $this->getDoctrine()->getRepository(Jeux::class);
        $findAll = $repo->findAll();
        $count = count($findAll);
        $paginator  = $this->get('knp_paginator');
        
        // Paginate the results of the query
        $jeux = $paginator->paginate(
            // Doctrine Query, not results
            $findAll,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );


        return $this->render("/gestion_jeux/listeJeux.html.twig",[
            'count' => $count,
            'jeux' => $jeux
        ]);
    }
}
