<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader 
{

    private $targetDirectory;

    public function __construct($targetDirectory){
        $this->targetDirectory = $targetDirectory;

    }

    public function Upload(UploadedFile $file){

        $fileName = $file->getClientOriginalName();
        
        $extension = array("jpeg","jpg","bmp","png","gif");
        $up = false;
        foreach($extension as $ext)
            {
                if($ext == $file->guessExtension())
                    $up = true ;
            }
        if(!$up)
            return $filname= array("up" =>false, "message" => "Le fichier n'est pas au bon format");
        else{    
             try {
                  $file->move(
                     $this->getTargetDirectory(),
                     $fileName
                 );
                 } catch (FileException $e) {
                     // ... handle exception if something happens during file upload
                      return $filename = array("up" => false, "message" => "une erreur est survenue lors de l'upload"); 
                  }

                  return $filename = array("up" => true, "filename" => $fileName,"message" => "le fichier est correctement uploadé");
        }   
    }

     public function getTargetDirectory(){
            return $this->targetDirectory;
     }   



}
